Improved introductory task using code analyzers, formatting utilities and CI/CD
Code files:
   project/coder.cpp
   project/coder.h

CI/CD - .gitlab-ci.yml

Makefile
cppcheck - line 88
clang-tidy - line 91

Format  utility -- clang-format 
(configuration file  .clang-format)

Analysis tool: clang-tidy, cppcheck 


The list of files which are allowed to be modified:
    project/coder.cpp
    project/coder.h
    Makefile

Other files will be restored on Jenkins CI side before each build.
