#include "coder.h"
#include <limits>
#include <stdexcept>

Coder::Coder() {
  _m_buf = nullptr;
  _m_size = 0;
}

Coder::Coder(const Coder& t) {
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  _m_buf = new char[_m_size = t._m_size];

  for (unsigned int i = 0; i < _m_size; i++) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    *(_m_buf + i) = *(t._m_buf + i);
  }
}

void Coder::set(char const* buf, unsigned int size) {
  if (buf == nullptr) {
    throw std::logic_error("Error input buf");
  }
  if (size <= 0 || size >= std::numeric_limits<unsigned int>::max()) {
    throw std::logic_error("Error input size");
  }

  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  _m_buf = new char[_m_size = size];

  for (unsigned int i = 0; i < size; i++) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    *(_m_buf + i) = *(buf + i);
  }
}

Coder& Coder::operator=(const Coder& t) {
  if (this == &t) {
    return *this;
  }
  // NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
  this->_m_buf = new char[_m_size = t._m_size];
  for (unsigned int i = 0; i < _m_size; i++) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    *(_m_buf + i) = *(t._m_buf + i);
  }

  return *this;
}

void Coder::encode() { ::encode(_m_buf, static_cast<int>(_m_size)); }

void Coder::decode() {
  unsigned int end = 3;
  while (_m_size > end) {
    end = end * 2 + 1;
  }

  for (unsigned int i = 0; i < end; i++) {
    ::encode(_m_buf, static_cast<int>(_m_size));
  }
}

Coder::~Coder() { delete[] _m_buf; }
