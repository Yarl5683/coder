// NOLINTNEXTLINE(llvm-header-guard) //bad header-guard name "CODER_H)
#ifndef CODER_H
// NOLINTNEXTLINE(llvm-header-guard)
#define CODER_H

void encode(char* buf, int size);

class Coder {
 public:
  Coder();
  Coder(const Coder& /*t*/);  //(readability-named-parameter

  Coder(Coder&&) = default;  // move constructor (hicpp-special-member-function)
  Coder& operator=(Coder&&) =
      default;  // move assigment operator(hicpp-special-member-function)

  Coder& operator=(const Coder& /*t*/);

  void set(const char* buf, unsigned int size);
  char* buf() const { return _m_buf; };
  unsigned int size() const { return _m_size; };

  void encode();
  void decode();

  ~Coder();

 private:
  char* _m_buf;
  unsigned int _m_size;
};

// NOLINTNEXTLINE(llvm-header-guard)
#endif  // CODER_H
